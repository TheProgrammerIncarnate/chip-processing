The AY-3-8606-1 "Wipeout" game chip IC

A wierd cross between pong & breakout

Created by General Instruments in ~1976

Read post about it here: https://nerdstuffbycole.blogspot.com/2019/03/

Original die images provided by Sean Riddle @(http://seanriddledecap.blogspot.com/2018/05/blog-post_7.html), licensed under CC BY-SA 4.0

Annotation and processing work was done by Cole Johnson, again liscensed under CC BY-SA 4.0

(https://creativecommons.org/licenses/by-sa/4.0/)