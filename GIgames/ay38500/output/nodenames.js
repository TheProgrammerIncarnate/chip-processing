/*
* Generated from source images by the ChipTools image processor
* Polygon and netlist source file of the AY-3-8500
* Based on die photos from Sean Riddle and highlighting done by Cole Johnson
* (http://seanriddledecap.blogspot.com/2017/02/blog-post.html)
* This work is licensed under the Creative Commons Attribution 3.0 United States License. 
* To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/us/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/
var nodenames = {
gnd: 921,
vcc: 920,
reset: 926,
clk: 530,
disable: 914,
internal_clock: 394,
half_clock: 883,
internal_reset: 916,
reset_latch: 830,
pinLPin: 198,
pinRPin: 68,
pinLPout: 3,
pinRPout: 2,
pinManualServe: 38,
pinBallSpeed: 321,
pinBallOut: 5,
pinBallAngle: 95,
pinSound: 431,
pinHitIn: 678,
pinShotIn: 866,
pinSFout: 922,
pinPractice: 925,
pinSquash: 924,
pinSoccer: 923,
pinTennis: 908,
pinRifle2: 864,
pinRifle1: 744,
pinSyncOut: 391,
pinBatSize: 236,
to_sync: 272,
hsyncNOT_ON2: 722,
hsyncON: 392,
hsyncNOT_ON: 225,
vsyncON: 223,
vsyncNOT_ON: 248,
hrz_shifter: 369,
vrt_shifter: 375,
hrz_reset: 370,
vrt_reset: 358,
hrz_feeder: 307,
vrt_feeder: 516,
hrzControl1: 399,
hrzControl2: 700,
hrzControl3: 661,
hrzControl4: 718,
hrzControl5: 717,
hrzControl6: 429,
hrzControl7: 697,
hrzControl8: 698,
hrzControl9: 664,
hrzControl10: 659,
hrzControl11: 660,
hrzControl12: 452,
hrzControl13: 367,
hrzControl14: 458,
NOT_hrzControl1: 689,
NOT_hrzControl6: 777,
NOT_hrzControl12: 688,
vrtControl1: 473,
vrtControl2: 481,
vrtControl3: 839,
vrtControl4: 843,
vrtControl5: 812,
vrtControl6: 840,
vrtControl7: 484,
vrtControl8: 489,
vrtControl9: 844,
vrtControl10: 455,};