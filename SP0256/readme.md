SP0256-012 Speech synthesis chip (this varient is from an Intellivoice module)

Created by GI around 1980. Any IP rights are now probally owned by Microchip Technology (www.microchip.com)

Die photos copyright of the visual6502 team (http://www.visual6502.org/images/pages/GeneralInstruments_SP0256_012_die_shots.html)

Original annotated image "GI_SP0256_die_shot_8500w_10.png" annotated by JohnPCAE (http://atariage.com/forums/topic/252738-intellivoice-sp0256-die-shot-cleaned-up/)

(Partially) Fixed annotated image is fixed.png

A python script was used to round some edges in the (fixed) annotated image before running through ChipTools

(Ongoing) corrections and processing done by Cole Johnson

This chip has been partially simulated! Find it here: (http://nerdydelights.droppages.com/ChipSim/SP0256/sp0256.html) Source: (https://gitlab.com/TheProgrammerIncarnate/chip-processing/tree/master/SP0256)